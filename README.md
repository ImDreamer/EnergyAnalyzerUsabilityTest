Usability test for [Energy Analyzer](https://gitlab.com/ImDreamer/analyzer-p10).

### Introduction
Today you will be testing a tool for optimizing the energy usage of C\#
programs. Namely an analyzer named *EnergyAnalyzer*. You will be handed
a list of tasks that we expect you to try to solve on your own while
thinking out loud. After you have solved all the tasks we will conduct a
short interview regarding other thoughts, improvements, and debriefing.
The moderator will be able to help you if you get stuck but otherwise
will only observe. By participating in this test you accept that we can
use the collected data in our university project. The data will be
analyzed anonymously. Thank you so much for your attention and
participation in advance.

-   Task 1

    -   Get an overview of the sample program.

-   Task 2

    -   Install the package using NuGet.

-   Task 3

    -   Count and say how many suggestions you can see.

-   Task 4

    -   Apply all code fixes.

-   Task 5

    -   Observe and explain how you would use the information provided
        to solve the remaining suggestions.
