using System.Collections;

namespace UsabilityTestAnalyzer;

internal class Point2D {
	public Int32 X { get; set; }
	public Int32 Y { get; set; }

	public Double Magnitude => Math.Sqrt(X * X + Y * Y);

	public override string ToString() {
		return $"({X}, {Y})";
	}
}

internal static class Example {
	public static void Main() {
		ArrayList points = new ArrayList();

		points.Add(new Point2D { X = 1, Y = 2 });
		points.Add(new Point2D { X = 3, Y = 4 });
		points.Add(new Point2D { X = 5, Y = 6 });
		points.Add(new Point2D { X = 21, Y = 4 });

		Double sumMagnitude = points.OfType<Point2D>().Sum(point2D => point2D.Magnitude);

		String result = "The " + nameof(sumMagnitude) + " of the points is" + sumMagnitude;
		Console.WriteLine(result);

		String count = "The list contains this amount of points:" + points.Count;
		Console.WriteLine(count);

		for (var index = 0; index < points.Count; index++) {
			try {
				var point = (Point2D)points[index];
				Console.WriteLine(point);
			}
			catch (Exception e) {
				Console.WriteLine(e);
				throw;
			}
		}

		var randomPoint = (Point2D)points[Random.Shared.Next(0, points.Count)];
		double magnitude = randomPoint!.Magnitude;

		int magnitudeDirection;
		if (magnitude > 1) {
			Console.WriteLine("magnitude > 1");
			magnitudeDirection = 0;
		}
		else if (magnitude > 0) {
			Console.WriteLine("magnitude > 0");
			magnitudeDirection = 1;
		}
		else if (magnitude == 0) {
			Console.WriteLine("magnitude == 1");
			magnitudeDirection = 2;
		}
		else {
			Console.WriteLine("magnitude < 1");
			magnitudeDirection = 3;
		}

		Console.WriteLine("magnitudeDirection: " + magnitudeDirection);
	}
}